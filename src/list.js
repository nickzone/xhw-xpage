(function () {

    var html = '<div class="list-wrapper"><ul class="text-list"></ul><div class="more-tip"></div></div>';

    // 将body改成需要的挂载元素
    $('.data-list').append(html);

    var nodeIdMap = {
        'dtxw': '11152431',
        'ltrd': '11152433',
        'djch': '11152434',
        'sdts': '11152435',
        'jdxw': '11152432',
        'zb': '11153490'
    };

    var moreTipState = {
        isHasMore: '<div class="tip-hasmore">向下滚动加载列表...</div>',
        isLoading: '<div class="tip-loading">正在载入，请稍等...</div>',
        isError: '<div class="tip-error">加载超时，请重试...</div>',
        isNoMore: '<div class="tip-nomore">没有更多了...</div>'
    };

    new XHW.Xpager('.text-list', {
        mode: 'pageView',
        nid: nodeIdMap[$('body').data('page')],
        pageSize: 10,
        pagination: '.more-tip',
        moreTip: '.more-tip',
        moreTipState: moreTipState,
        renderItemAsyn: function (item, index, list, cb) {
            var li = '<li class="text-item">' + '<a href="' + item.LinkUrl + '">' + item.Title + '</a></li>';
            cb(li);
        }
    });
})();