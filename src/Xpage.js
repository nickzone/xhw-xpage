/**
 * 一款基于翔宇的通用分页器组件
 * author: wangxiaokang
 * build: 2017-05-16
 * Contact: 524920914@qq.com
 */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['$'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory($);
    } else {
        var XHW = window.XHW = window.XHW || {};
        XHW.Xpage = factory(window.Zepto || window.jQuery || $);
    }
})(this, function ($) {
    // 内部函数，用来按需执行回调
    function _runCb(cb, params, context) {
        if (cb && typeof cb === 'function') {
            cb.apply(context, params.join ? params : [params]);
        }
    }
    // 翔宇列表加载组件
    function Xpage(selector, opts) {
        var xPage = this;
        // 默认配置
        var defaults = {
            mode: 'listView', //raw,listView,listView-scroll,pageView
            // nid: '11135737', // 节点id, 必选
            item: 'li', // 列表选择器 ,可选,默认值：li
            pageSize: 10, // 每页条数,可选,默认值：10
            maxPage: 10000, //最大页码，可选
            // moreButton: null, // 瀑布流模式使用的“加载更多按钮”
            // moreTip: null,  // 显示加载状态的提示区域
            // pagination: null, // 分页模式使用的“页码容器”
            // scrollWrapper: null, // listView-scroll滚动容器,可选,默认为window
            // 加载更多按钮状态显示
            moreButtonState: {
                isEmpty: '暂无数据',
                isHasMore: '查看更多',
                isLoading: '加载中...',
                isError: '出错，点击重试',
                isNoMore: '没有更多了'
            },
            moreTipState: {
                isEmpty: '暂无数据',
                isHasMore: '滚动到底部加载',
                isLoading: '加载中...',
                isError: '出错，请重试',
                isNoMore: '没有更多了'
            }
            // 同步渲染
            // renderItem: function (item, index, list) {
            //     console.log('renderItem');
            // },
            // 异步渲染，在适当的时候调用cb(item),即可异步插入列表，当列表项需要异步获取额外数据后拼接显示时可用
            // renderItemAsyn: function (item, index, list, cb) {

            // },
            // 列表初始化时被调用（已经获取了必要的分页信息）
            // onInit: function (xpage) {
            //     console.log('onInit:', this.getTotal(), ',', this.getTotalPage(), ',', this.getPage(), ',', this.getStatus());
            // },
            // 加载状态监听函数，0：空列表 1：可加载 ，2：加载中:， 3：加载超时 4：已全部加载
            // onStatusChange: function (status) {
            //     console.log('onStatusChange:', this.getTotal(), ',', this.getTotalPage(), ',', this.getPage(), ',', this.getStatus());
            // },
            // 页码切换时调用：page 当前页
            // onPageChange: function (page) {
            //     console.log('onPageChange:', this.getTotal(), ',', this.getTotalPage(), ',', this.getPage(), ',', this.getStatus());
            // },
            // 监听分页数据
            // onPageRender: function (data) {
            //     console.log('onPageRender:', this.getTotal(), ',', this.getTotalPage(), ',', this.getPage(), ',', this.getStatus());
            //     console.log(data);
            // }
        };
        // 分页数据模型
        var model = this.model = $.extend({}, defaults, opts);
        // 允许最大设置50条
        model.pageSize = model.pageSize > 50 ? 50 : model.pageSize;
        // 当前页序号
        model.page = 0;
        // 总条数：由ajax获取或本地计算得出
        model.total = 0;
        // 总页码：由total/pageSize计算得出
        model.totalPage = 0;
        // 加载状态
        model.status = 1;
        // 列表容器
        model.$pageContainer = $(selector);
        // 更多按钮
        model.$moreButton = $(model.moreButton);
        // 滚动提示
        model.$moreTip = $(model.moreTip);
        // 滚动容器
        model.$scrollWrapper = $(model.scrollWrapper);
        // 翔宇列表
        model.$pageItems = model.$pageContainer.find(model.item);
        // 分页器组件
        model.$pagination = $(model.pagination);
        // 翔宇实际加载总条数
        model.xyTotal = model.$pageItems.length;
        // 翔宇实际加载总页数
        model.xyPageCount = Math.ceil(model.xyTotal / model.pageSize);
        // 缓存数据，减少重复加载
        model.cache = [];
        // 设置当前页码
        model.setPage = function (page) {
            var
                model = this,
                totalPage = model.totalPage;

            model.page = Math.min(page, totalPage);
            model.setStatus();

            // 执行组件内部、外部定义的回调函数
            _runCb(xPage.routers[model.mode].onPageChange, model.page, xPage);
            _runCb(model.onPageChange, model.page, xPage);
        };
        model.setStatus = function (status) {
            var model = this;

            if (status !== undefined) {
                model.status = status;
            } else if (model.page < model.totalPage) {
                model.status = 1;
            } else {
                model.status = 4;
            }

            _runCb(xPage.routers[model.mode].onStatusChange, model.status, xPage);
            _runCb(model.onStatusChange, model.status, xPage);
        };
        model.setTotal = function (total) {
            var model = this,
                pageSize = model.pageSize;

            model.total = total;
            model.setTotalPage(Math.ceil(total / pageSize));
        };
        model.setTotalPage = function (count) {
            var model = this;

            model.totalPage = count;
            _runCb(xPage.routers[model.mode].onTotalPageChange, model.totalPage, xPage);
            _runCb(model.onTotalPageChange, model.totalPage, xPage);
        };

        // 初始化组件
        this.init();
    }

    Xpage.prototype = {
        constructor: Xpage,
        routers: {},
        // 初始化入口函数
        init: function () {
            var
                self = this,
                model = self.model,
                mode = model.mode;

            _runCb(self.routers[mode].onStatusChange, 2, self);

            // 清空列表
            model.$pageItems.remove();
            // 获取列总数
            self.getAndSetTotal();
        },
        getAndSetTotal: function () {
            var self = this,
                model = self.model,
                pageSize = model.pageSize,
                xyPageCount = model.xyPageCount,
                maxPage = model.maxPage,
                mode = model.mode;

            self.loadRemoteData(1, function (data) {
                if (data.status == '-1') {
                    setEmpty();
                    return;
                }
                // 设置总数
                model.setTotal(Math.min(data.totalnum,maxPage * pageSize));
                // 执行init函数
                _runCb(model.onInit, self, self);
                // 渲染首屏
                self.routers[mode].render.call(self);

            }, function (err) {
                setEmpty();
            });

            function setEmpty() {
                model.setStatus(0);
                model.setTotal(Math.min(xyTotal,maxPage * pageSize));
            }
        },
        getTotal: function () {
            return this.model.total;
        },
        getTotalPage: function () {
            return this.model.totalPage;
        },
        getPage: function () {
            return this.model.page;
        },
        getStatus: function () {
            return this.model.status;
        },
        // 加载当前页数据
        goToPage: function (pageNum) {
            var self = this,
                model = self.model,
                totalPage = model.totalPage,
                mode = model.mode;

            pageNum = Math.min(Math.max(1, pageNum), totalPage);

            function onSuccess(data) {
                // 页码加1
                model.setPage(pageNum);
                _runCb(self.routers[mode].onPageRender, data, self);
                _runCb(model.onPageRender, data, self);
            }
            function onFail(err) {
                model.setStatus(3);
            }

            if (self.isReadyForLoad(pageNum)) {
                // 加载状态
                model.setStatus(2);
                // 渲染页面
                if (self.isRenderByXy(pageNum)) {
                    self.renderByXY(pageNum, onSuccess);
                } else {
                    self.renderByJSON(pageNum, onSuccess, onFail);
                }
            }
        },
        nextPage: function () {
            this.goToPage(this.model.page + 1);
        },
        prePage: function () {
            this.goToPage(this.model.page - 1);
        },
        firstPage: function () {
            this.goToPage(1);
        },
        lastPage: function () {
            this.goToPage(this.model.totalPage);
        },
        // 用翔宇加载下一页
        renderByXY: function (pageNum, success) {
            var
                self = this,
                model = self.model,
                pageSize = model.pageSize,
                $pageItems = model.$pageItems,
                $itemsHTML = $pageItems.slice(pageSize * (pageNum - 1), pageSize * pageNum),
                listHtml = '';
            
            for (var i = 0; i < $itemsHTML.length; i++) {
                var item = $itemsHTML.get(i);
                listHtml 　+= item.outerHTML;
            }
            
            setTimeout(function () {
                _runCb(success, listHtml, self);
            }, 100);
        },
        // 用JSONP加载下一页
        renderByJSON: function (pageNum, success, fail) {
            var self = this,
                model = self.model,
                mode = model.mode,
                itemsHTML = [],
                fishedCount = 0,
                listLen;
            // 加载远程数据
            this.loadRemoteData(pageNum, loadSuccess, LoadFail);

            // 加载成功
            function loadSuccess(data) {
                var items = data.data.list;

                listLen = items.length;

                if (model.renderItemAsyn) {
                    for (var i = 0; i < listLen; i++) {
                        model.renderItemAsyn.call(self, items[i], i, items, renderItem(i));
                    }
                } else {
                    for (var j = 0; j < listLen; j++) {
                        itemsHTML.push(model.renderItem.call(self, items[j], j, items));
                    }
                    success(itemsHTML.join(''));
                }
            }

            // 加载失败
            function LoadFail(err) {
                fail(err);
            }

            // 渲染列表
            function renderItem(i) {
                return function renderHTML(itemHTML) {
                    itemsHTML[i] = itemHTML;
                    fishedCount++;
                    if (fishedCount === listLen) {
                        success(itemsHTML.join(''));
                    }
                };
            }
        },
        // JSONP执行逻辑
        loadRemoteData: function (pageNum, success, fail) {
            var
                model = this.model,
                nid = model.nid,
                cnt = model.pageSize,
                attr = model.attr === undefined ? '': model.attr,
                orderby = model.orderby === undefined ? 0 : model.orderby,
                tp = model.tp === undefined ? 1 : model.tp,
                pgnum = pageNum,
                url = 'http://qc.wa.news.cn/nodeart/list?',
                listCahce = model.cache[pageNum - 1];
            
            if (listCahce) {
                return success( {
                    "data": {
                        "list": listCahce
                    }
                });
            }

            url += [
                'nid=' + nid,
                'pgnum=' + pgnum,
                'cnt=' + cnt,
                'attr=' + attr,
                'tp=' + tp,
                'orderby=' + orderby
            ].join('&');

            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'jsonp',
                success: function (data) {
                    // 缓存数据
                    if (data.data && data.data.list) {
                        model.cache[pageNum - 1] = data.data.list;
                    }
                    success(data);
                },
                error: fail,
                context: this
            });

            // console.log(url);
        },
        // 是否已经加载到最后一页
        isLoadAll: function () {
            var model = this.model;
            return model.page + 1 <= model.totalPage ? false : true;
        },
        // 是否含有XY模板
        isXY: function () {
            return this.model.xyPageCount > 0;
        },
        // 是否还在翔宇加载阶段
        isRenderByXy: function (pageNum) {
            return pageNum <= this.model.xyPageCount;
        },
        isReadyForLoad: function (pageNum) {
            var model = this.model;
            var mode = model.mode;
            var page = model.page;
            var totalPage = model.totalPage;
            var shouldLoad = false;

            if (pageNum <= model.totalPage && model.status !== 2 && pageNum !== page) {
                shouldLoad = true;
            }

            return shouldLoad;
        }
    };

    // 原始模式 
    var raw = Xpage.prototype.routers.raw = {
        render: function () {
            var self = this,
                mode = self.model.mode;

            self.goToPage(1);
            self.routers[mode].listen && self.routers[mode].listen.call(self);
        }
    };

    // 列表模式
    var listView = Xpage.prototype.routers.listView = $.extend({}, raw, {
        listen: function () {
            var self = this,
                model = self.model;

            model.$moreButton.on('click', function (e) {
                e.preventDefault();
                self.nextPage();
            });
        },
        onPageRender: function (list) {
            this.model.$pageContainer.append(list);
        },
        onStatusChange: function (status) {
            var
                tipText,
                self = this,
                model = self.model,
                $moreButton = model.$moreButton,
                moreButtonState = model.moreButtonState;

            switch (status) {
                case 0:
                    tipText = moreButtonState.isEmpty;
                    break;
                case 1:
                    tipText = moreButtonState.isHasMore;
                    break;
                case 2:
                    tipText = moreButtonState.isLoading;
                    break;
                case 3:
                    tipText = moreButtonState.isError;
                    break;
                case 4:
                    tipText = moreButtonState.isNoMore;
                    break;
            }

            $moreButton.html(tipText);
        },
        onPageChange: function (page) {
            // console.log('inner-page:' + page);
        }
    });

    // 滚动列表    
    var listViewScroll = Xpage.prototype.routers['listView-scroll'] = $.extend({}, listView, {
        listen: function () {
            var t,
                self = this,
                model = self.model,
                isDefault = model.$scrollWrapper.length === 0,
                $wrapper = isDefault ? $(window) : model.$scrollWrapper;

            $wrapper.on('scroll', showNextPage);

            function showNextPage(e) {
                clearTimeout(t);
                t = setTimeout(function () {
                    var
                        target = e.target,

                        scrollTop = isDefault ?
                            document.body.scrollTop || document.documentElement.scrollTop :
                            target.scrollTop,

                        clientHeight = isDefault ?
                            document.documentElement.clientHeight :
                            target.clientHeight,

                        scrollHeight = isDefault ?
                            document.documentElement.scrollHeight :
                            target.scrollHeight;

                    if (scrollHeight - clientHeight - scrollTop < 50) {
                        self.nextPage();
                    }

                }, 500);
            }
        },
        onStatusChange: function (status) {
            var
                tipText,
                self = this,
                model = self.model,
                $moreTip = model.$moreTip,
                moreTipState = model.moreTipState;

            switch (status) {
                case 0:
                    tipText = moreTipState.isEmpty;
                    break;
                case 1:
                    tipText = moreTipState.isHasMore;
                    break;
                case 2:
                    tipText = moreTipState.isLoading;
                    break;
                case 3:
                    tipText = moreTipState.isError;
                    break;
                case 4:
                    tipText = moreTipState.isNoMore;
                    break;
            }

            $moreTip.html(tipText);
        },

    });

    // 分页模式 
    var pageView = Xpage.prototype.routers.pageView = $.extend({}, raw, {
        listen: function () {
            var self = this,
                model = self.model,
                totalPage = model.totalPage,
                $pagination = model.$pagination;

            pageView.makePagination.call(self, 1, totalPage, $pagination);

            $pagination.on('click', function (e) {
                e.preventDefault();

                var $target = $(e.target);
                var type = $target.data('type');

                switch (type) {
                    case 'pager':
                        var pageNum = +$target.text();
                        self.goToPage(pageNum);
                        break;
                    case 'pre':
                        self.prePage();
                        break;
                    case 'first':
                        self.firstPage();
                        break;
                    case 'next':
                        self.nextPage();
                        break;
                    case 'last':
                        self.lastPage();
                        break;
                    default:
                }
            });
        },
        onPageRender: function (list) {
            this.model.$pageContainer.html(list);
        },
        onPageChange: function (page) {
            var self = this,
                model = self.model,
                totalPage = model.totalPage,
                $pagination = model.$pagination;

            pageView.makePagination(page, totalPage, $pagination);
        }
    });

    //  
    pageView.tpl = {
        first: '<span class="xpage-pagination-first"><a data-type="first">首页</a></span>',
        last: '<span class="xpage-pagination-last"><a data-type="last">尾页</a></span> ',
        pre: '<span class="xpage-pagination-pre"><a data-type="pre">&lt;</a></span> ',
        next: '<span class="xpage-pagination-next"><a data-type="next">&gt;</a></span> ',
        pager: '<span class="xpage-pagination-pager"><a data-type="pager">{{num}}</a></span>',
        pagerActive: '<span class="xpage-pagination-pager xpage-pagination-pager-active"><a data-type="active">{{num}}</a></span>'
    };

    pageView.makePagination = function (page, totalPage, $pagination) {
        var pagers = [];
        var perPagerNum = 10;
        var singleDigit = page % perPagerNum;
        var lowIndex = 1 + page - (singleDigit === 0 ? perPagerNum : singleDigit);

        // 插入页码
        for (var i = 0; i < perPagerNum && (lowIndex + i <= totalPage); i++) {
            var _page = lowIndex + i;
            if (_page === page) {
                pagers.push(makePager(lowIndex + i, 'pagerActive'));
            } else {
                pagers.push(makePager(lowIndex + i, 'pager'));
            }
        }

        // 插入上一页/首页
        if (page > 1) {
            pagers.unshift(makePager(null, 'pre'));
            pagers.unshift(makePager(null, 'first'));
        }


        // 插入下一页/尾页
        if (page < totalPage) {
            pagers.push(makePager(null, 'next'));
            pagers.push(makePager(null, 'last'));
        }

        $pagination.html(pagers.join(''));

        // 制作页码
        function makePager(page, type) {
            var tpl = pageView.tpl;

            switch (type) {
                case 'pagerActive':
                case 'pager':
                    return tpl[type].replace('{{num}}', page);
                default:
                    return tpl[type];
            }
        }
    };

    return Xpage;
});