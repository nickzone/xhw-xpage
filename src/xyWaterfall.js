$(document).ready(function ($) {
    // 加载更多功能
    function XyWaterfall(selector, opts) {

        var $thumbList = $(selector), // 列表
            $thumbItems = $thumbList.find(opts.item || 'li'), //列表项目
            $moreBtn = $(opts.moreBtn); // 更多按钮

        var maxNumber = 10000,
            xyTotal = $thumbItems.length || 0, // 翔宇总条数
            loadNumber = xyTotal,
            pageSize = opts.pageSize || 10, // 每页条数
            xyPageNum = opts.xyPageNum || 0, // 翔宇显示最大页数
            curPage = 1, // 当前页码
            isxy = (xyPageNum !== 0 && $thumbItems.length > 0) ? true : false, // 是否为翔
            xyTotalPageNum = Math.ceil(xyTotal / pageSize), // 翔宇总页数
            loadComplete = false, // 状态：加载完成
            isLoading = false; // 状态：加载中

        if (opts.maxNumber) {
            maxNumber = opts.maxNumber > xyTotal ? opts.maxNumber : xyTotal;
        }
        // 加载按钮状态管理
        var MORE_BTN_STATE = {
            HAS_MORE: '加载更多 &#187;',
            NO_MORE: '没有更多了',
            IS_LOADING: '数据加载中...',
            IS_ERROR: '重新加载'
        };

        // 加载远程数据
        function loadRemoteData(reqParams) {
            var nid = reqParams.nid,
                pgnum = reqParams.pagnum,
                cnt = reqParams.cnt,
                attr = reqParams.attr,
                url = 'http://qc.wa.news.cn/nodeart/list?';

            url += ['nid=' + nid, 'pgnum=' + pgnum, 'cnt=' + cnt, 'attr=' + attr, 'tp=1', 'orderby=1'].join('&');

            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'jsonp',
                timeout: 10000,
                success: onLoadSuccess,
                error: onLoadError,
            });

            // 远程数据加载成功的回调函数
            function onLoadSuccess(data) {
                if (data.status === '-1' || loadNumber >= maxNumber) { // 数据为空
                    loadComplete = true;
                    $moreBtn.html(MORE_BTN_STATE.NO_MORE);
                    return;
                }

                loadNumber = loadNumber + pageSize;

                var listData = data.data.list;

                if (loadNumber >= maxNumber) {
                    listData = listData.slice(0, maxNumber - (loadNumber - pageSize));
                }

                var itemHTML = '';
                // 渲染模板
                for (var i = 0; i < listData.length; i++) {
                    itemHTML += opts.render(listData[i], i, listData);
                }
                $thumbList.append(itemHTML); // 渲染dom

                isLoading = false; // 加载完成
                if (data.totalnum <= pageSize * curPage) { // 加载完所有条目
                    loadComplete = true;
                }
                if (!loadComplete) {
                    $moreBtn.html(MORE_BTN_STATE.HAS_MORE);
                } else {
                    $moreBtn.html(MORE_BTN_STATE.NO_MORE);
                }
            }

            // 远程数据加载失败的回调函数
            function onLoadError() {
                isLoading = false; // 加载完成
                curPage = curPage - 1; //回退到加载失败;
                $moreBtn.html(MORE_BTN_STATE.IS_ERROR);
            }

        }

        function loadJsonPage() {
            isLoading = true;
            $moreBtn.html(MORE_BTN_STATE.IS_LOADING);
            // 加载当前页
            loadRemoteData({
                nid: opts.nid, // id
                pagnum: curPage, // 页码
                cnt: pageSize, // 每页数量
                attr: opts.attr || '' // 节点类型
            });

        }

        if (isxy) { // 翔宇 + json
            if (xyTotal <= pageSize) {
                $moreBtn.html(MORE_BTN_STATE.NO_MORE);
            } else {
                // 初始化列表状态
                $thumbItems.slice(pageSize).hide();
                $moreBtn.html(MORE_BTN_STATE.HAS_MORE);

                // 加载更多功能
                $moreBtn.on('click', function (e) {
                    e.preventDefault();
                    if (loadComplete || isLoading) return; // 加载完成或加载中，不响应点击事件
                    // 前进一页
                    curPage = curPage + 1;
                    // 执行翔宇加载部分
                    if (curPage <= xyTotalPageNum && curPage <= xyPageNum) {
                        // 显示当前页
                        $thumbItems.slice(pageSize * (curPage - 1) - 1, pageSize * curPage).show();
                        // 翔宇加载完
                        if (curPage === xyTotalPageNum && curPage < xyPageNum * pageSize) {
                            $moreBtn.html(MORE_BTN_STATE.NO_MORE);
                            loadComplete = true;
                        } else {
                            $moreBtn.html(MORE_BTN_STATE.HAS_MORE);
                        }
                    } else { // 执行JSON加载过程

                        loadJsonPage();

                    }
                });
            }
        } else { // 只有json

            loadJsonPage(); // 加载第一页
            $moreBtn.on('click', function (e) {
                e.preventDefault();
                // 前进一页
                curPage = curPage + 1;
                if (loadComplete || isLoading) return; // 加载完成或加载中，不响应点击事件
                loadJsonPage();
            });
        }
    }
    var XHW = window.XHW = window.XHW || {};
    XHW.XyWaterfall = XyWaterfall;
});